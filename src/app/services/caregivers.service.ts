import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Caregiver } from '../models/Caregiver';

@Injectable({
  providedIn: 'root'
})
export class CaregiversService {
  private url: string = "/webapi/caregivers"; 

  constructor(private http: HttpClient) { }

  getCaregivers(): Observable<Caregiver[]>{
    return this.http.get<Caregiver[]>(this.url).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  insertCaregiver(caregiver: Caregiver): Observable<Caregiver> {
    return this.http.post<Caregiver>(this.url + '/insert', caregiver);
  }

  deleteCaregiver(username: string){
    return this.http.delete(this.url + "/delete/" + username);
  }

  editCaregiver(caregiver: Caregiver): Observable<Caregiver> {
    return this.http.put<Caregiver>(this.url + '/update', caregiver);
  }
}
