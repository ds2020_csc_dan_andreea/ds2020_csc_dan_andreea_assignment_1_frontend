import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject!: BehaviorSubject<any>;
  public currentUser!: Observable<any>; 
  private url: string = "http://localhost:8070/webapi"; 

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')!));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  login(username: string, password: string) {
    return this.http.post<any>(this.url + `/users/login`, { username, password })
      .pipe(map((user: any) => {
        // store user details in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        console.log(JSON.parse(localStorage.getItem('currentUser')!));
        return user;
      }));
  }

  public getRole(): string {
    if (localStorage.getItem('currentUser') == null) {
      return '';
    }
    return JSON.parse(localStorage.getItem('currentUser')!)['role'];
  }

  logout(): void {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

}
