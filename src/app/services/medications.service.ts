import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { catchError, tap } from 'rxjs/operators';
import { Medication } from '../models/Medication';

@Injectable({
  providedIn: 'root'
})
export class MedicationsService {

  private url: string = "/webapi/medications"; 

  constructor(private http: HttpClient) { }

  getMedications(): Observable<Medication[]>{
    return this.http.get<Medication[]>(this.url).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  insertMedication(medication: Medication): Observable<Medication> {
    return this.http.post<Medication>(this.url + '/insert', medication);
  }

  updateMedication(medication: Medication): Observable<Medication> {
    return this.http.put<Medication>(this.url + '/update', medication);
  }

  deleteMedication(name: string){
    return this.http.delete(this.url + "/delete/" + name);
  }
}

