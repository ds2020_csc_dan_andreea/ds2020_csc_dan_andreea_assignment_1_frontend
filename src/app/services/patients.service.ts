import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Patient } from '../models/Patient';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  private url: string = "/webapi/patients"; 

  constructor(private http: HttpClient) { }

  getPatientsForCaregiver(): Observable<Patient[]>{
    var caregiver_username = JSON.parse(localStorage.getItem("currentUser")!)['username'];
    return this.http.get<Patient[]>(this.url + "/caregiver/" + caregiver_username).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getPatients():Observable<Patient[]>{
    return this.http.get<Patient[]>(this.url).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  insertPatient(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>(this.url + '/insert', patient);
  }

  deletePatient(username: string){
    return this.http.delete(this.url + "/delete/" + username);
  }

  editPatient(patient: Patient): Observable<Patient> {
    return this.http.put<Patient>(this.url + '/update', patient);
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
