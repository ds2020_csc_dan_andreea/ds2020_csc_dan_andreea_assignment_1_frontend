import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { TreatmentPlan } from '../models/TreatmentPlan';

@Injectable({
  providedIn: 'root'
})
export class TreatmentsService {

  private url: string = "/webapi/treatments"; 

  constructor(private http: HttpClient) { }

  getTreatmentPlansForPatient(): Observable<TreatmentPlan[]> {
    var patient_username = JSON.parse(localStorage.getItem("currentUser")!)['username'];
    return this.http.get<TreatmentPlan[]>(this.url + "/patients/" + patient_username).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getAllTreatmentPlans(): Observable<TreatmentPlan[]>{
    return this.http.get<TreatmentPlan[]>(this.url).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  insertTreatment(treatment: TreatmentPlan): Observable<TreatmentPlan> {
    return this.http.post<TreatmentPlan>(this.url + '/insert', treatment);
  }
}
