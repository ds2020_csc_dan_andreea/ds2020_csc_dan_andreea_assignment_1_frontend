import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Login } from '../models/Login';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username!: string;
  password!: string;
  hide: boolean = true;
  loggedUser!: Login;

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.loggedUser = new Login();
  }

  login(event: any): void {
    event.preventDefault();
    this.authenticationService.login(this.username, this.password)
      .pipe(first())
      .subscribe({
        next: () => {
          let role = this.authenticationService.getRole();
          console.log(localStorage.getItem('currentUser'));
          switch (role) {
            case ("doctor").valueOf():
              this.router.navigate(['/doctor']); // route to doctor view
              break;
            case ("patient").valueOf():
              this.router.navigate(['/patient']); // route to patient view
              break;
            case ("caregiver").valueOf():
              this.router.navigate(['/caregiver']); // route to caregiver view
              break;
            default:
              this.router.navigate(['/navbar']);
          }
        },
        error: error => {
          console.log("Error in Login Component, login");
        }
      });
  }

}
