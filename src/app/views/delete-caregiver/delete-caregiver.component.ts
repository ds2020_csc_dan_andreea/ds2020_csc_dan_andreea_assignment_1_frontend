import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CaregiversService } from 'src/app/services/caregivers.service';

@Component({
  selector: 'app-delete-caregiver',
  templateUrl: './delete-caregiver.component.html',
  styleUrls: ['./delete-caregiver.component.css']
})
export class DeleteCaregiverComponent implements OnInit {

  username: string="";

  constructor(public dialogRef: MatDialogRef<DeleteCaregiverComponent>,
    private caregiverService: CaregiversService) { }

  ngOnInit(): void {
  }

  deleteCaregiver(){
    this.dialogRef.close();
    this.caregiverService.deleteCaregiver(this.username).subscribe();
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
