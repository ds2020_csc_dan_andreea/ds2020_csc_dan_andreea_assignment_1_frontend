import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCaregiverComponent } from './delete-caregiver.component';

describe('DeleteCaregiverComponent', () => {
  let component: DeleteCaregiverComponent;
  let fixture: ComponentFixture<DeleteCaregiverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteCaregiverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCaregiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
