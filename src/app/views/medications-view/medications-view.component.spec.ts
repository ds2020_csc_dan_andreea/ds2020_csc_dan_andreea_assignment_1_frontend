import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicationsViewComponent } from './medications-view.component';

describe('MedicationsViewComponent', () => {
  let component: MedicationsViewComponent;
  let fixture: ComponentFixture<MedicationsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicationsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
