import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Medication } from 'src/app/models/Medication';
import { MedicationsService } from 'src/app/services/medications.service';
import { DeleteMedicationComponent } from '../delete-medication/delete-medication.component';
import { EditMedicationComponent } from '../edit-medication/edit-medication.component';
import { NewMedicationComponent } from '../new-medication/new-medication.component';

@Component({
  selector: 'app-medications-view',
  templateUrl: './medications-view.component.html',
  styleUrls: ['./medications-view.component.css']
})
export class MedicationsViewComponent implements OnInit {

  error!: string;
  displayedColumns: string[] = ['id', 'name', 'dosage', 'sideEffects'];
  dataSource = new MatTableDataSource<Medication>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private medicationsService: MedicationsService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.populateMedications();
    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  populateMedications(){
    this.medicationsService.getMedications().subscribe(
      (medications: Medication[])=> {this.setMedicationsTable(medications);},
      (err: any) => console.log(err)
    )
  }

  setMedicationsTable(medications: Medication[]){
    this.dataSource = new MatTableDataSource<Medication>(medications);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  insertMedication(){
    let dialogCreate = this.dialog.open(NewMedicationComponent, {
      width: '400px',
      data: {},
    });
    dialogCreate.afterClosed().subscribe(result => { this.ngOnInit() });
}

deleteMedication(){
  let dialogDelete = this.dialog.open(DeleteMedicationComponent, {
    width: '400px',
    data: {},
  });
  dialogDelete.afterClosed().subscribe(result => { this.ngOnInit() });
}
updateMedication(){
  let dialogUpdate = this.dialog.open(EditMedicationComponent, {
    width: '400px',
    data: {},
  });
  dialogUpdate.afterClosed().subscribe(result => { this.ngOnInit() });

}
}
