import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Medication } from 'src/app/models/Medication';
import { MedicationsService } from 'src/app/services/medications.service';
import { NewMedicationComponent } from '../new-medication/new-medication.component';

@Component({
  selector: 'app-edit-medication',
  templateUrl: './edit-medication.component.html',
  styleUrls: ['./edit-medication.component.css']
})
export class EditMedicationComponent implements OnInit {

  name: string="";
  dosage: string="";
  sideEffects: string="";

  constructor(public dialogRef: MatDialogRef<NewMedicationComponent>,
    private medicationService: MedicationsService) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dialogRef.close();
  }

  editMedication(){
    let newMedication = new Medication();
    newMedication.name = this.name;
    newMedication.dosage = this.dosage;
    newMedication.sideEffects = this.sideEffects.split(',');
   
    this.closeDialog();
    return this.medicationService.updateMedication(newMedication).subscribe();
  }

}
