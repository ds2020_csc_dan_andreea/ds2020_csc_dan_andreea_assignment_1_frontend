import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TreatmentPlan } from 'src/app/models/TreatmentPlan';
import { TreatmentsService } from 'src/app/services/treatments.service';
import { NewTreatmentComponent } from '../new-treatment/new-treatment.component';

@Component({
  selector: 'app-treatment-view',
  templateUrl: './treatment-view.component.html',
  styleUrls: ['./treatment-view.component.css']
})
export class TreatmentViewComponent implements OnInit {

  role!: string;
  error!: string;
  treatments: TreatmentPlan[] = [];
  displayedColumns: string[] = ['firsthour', 'secondhour', 'period', 'medications'];
  dataSource = new MatTableDataSource<TreatmentPlan>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private treatmentService: TreatmentsService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.role=JSON.parse(localStorage.getItem('currentUser')!)['role'];

    if(this.role=='patient'){
      this.populatePatientTreatments();
    }
    else if(this.role=='doctor'){
      this.populateTreatments();
    }

    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  populatePatientTreatments(){
    this.treatmentService.getTreatmentPlansForPatient().subscribe(
      (treatments: TreatmentPlan[]) => { this.setTreatmentsTable(treatments); },
      (err: any) => console.log(err)
    );
  }

  populateTreatments(){
    this.treatmentService.getAllTreatmentPlans().subscribe(
      (treatments: TreatmentPlan[]) => { this.setTreatmentsTable(treatments); },
      (err: any) => console.log(err)
    );
  }

  setTreatmentsTable(treatments: TreatmentPlan[]){
    this.dataSource = new MatTableDataSource<TreatmentPlan>(treatments);
    }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  createTreatmentPlan(){
      let dialogCreate = this.dialog.open(NewTreatmentComponent, {
        width: '400px',
        data: {},
      });
      dialogCreate.afterClosed().subscribe(result => { this.ngOnInit() });
  }
}
