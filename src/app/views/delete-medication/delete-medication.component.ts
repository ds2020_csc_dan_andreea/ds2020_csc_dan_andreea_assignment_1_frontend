import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MedicationsService } from 'src/app/services/medications.service';

@Component({
  selector: 'app-delete-medication',
  templateUrl: './delete-medication.component.html',
  styleUrls: ['./delete-medication.component.css']
})
export class DeleteMedicationComponent implements OnInit {

  name: string="";

  constructor(public dialogRef: MatDialogRef<DeleteMedicationComponent>,
    private medicationService: MedicationsService) { }

  ngOnInit(): void {
  }

  deleteMedication(){
    this.dialogRef.close();
    this.medicationService.deleteMedication(this.name).subscribe();
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
