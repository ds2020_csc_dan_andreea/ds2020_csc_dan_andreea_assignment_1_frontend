import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Caregiver } from 'src/app/models/Caregiver';
import { CaregiversService } from 'src/app/services/caregivers.service';
import { DeleteCaregiverComponent } from '../delete-caregiver/delete-caregiver.component';
import { DeletePatientComponent } from '../delete-patient/delete-patient.component';
import { EditCaregiverComponent } from '../edit-caregiver/edit-caregiver.component';
import { NewCaregiverComponent } from '../new-caregiver/new-caregiver.component';

@Component({
  selector: 'app-caregivers-view',
  templateUrl: './caregivers-view.component.html',
  styleUrls: ['./caregivers-view.component.css']
})
export class CaregiversViewComponent implements OnInit {

  error!: string;
  caregivers: Caregiver[] = [];
  displayedColumns: string[] = ['id', 'address', 'birthdate','gender','username','name'];
  dataSource = new MatTableDataSource<Caregiver>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private caregiversService: CaregiversService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.populateCaregivers();
    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  populateCaregivers(){
    this.caregiversService.getCaregivers().subscribe(
      (caregivers: Caregiver[])=> {this.setCaregiversTable(caregivers);},
      (err: any) => console.log(err)
    )
  }

  setCaregiversTable(caregivers: Caregiver[]){
    this.dataSource = new MatTableDataSource<Caregiver>(caregivers);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
  insertCaregiver(){
    let dialogCreate = this.dialog.open(NewCaregiverComponent, {
      width: '400px',
      data: {},
    });
    dialogCreate.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  deleteCaregiver(){
    let dialogCreate = this.dialog.open(DeleteCaregiverComponent, {
      width: '400px',
      data: {},
    });
    dialogCreate.afterClosed().subscribe(result => {this.ngOnInit()});
  }

  updateCaregiver(){
    let dialogCreate = this.dialog.open(EditCaregiverComponent, {
      width: '400px',
      data: {},
    });
    dialogCreate.afterClosed().subscribe(result => {this.ngOnInit()});
  }
}
