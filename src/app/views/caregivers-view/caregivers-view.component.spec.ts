import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiversViewComponent } from './caregivers-view.component';

describe('CaregiversViewComponent', () => {
  let component: CaregiversViewComponent;
  let fixture: ComponentFixture<CaregiversViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaregiversViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiversViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
