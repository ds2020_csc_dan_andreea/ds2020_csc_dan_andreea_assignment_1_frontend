import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-delete-patient',
  templateUrl: './delete-patient.component.html',
  styleUrls: ['./delete-patient.component.css']
})
export class DeletePatientComponent implements OnInit {

  username: string="";

  constructor(public dialogRef: MatDialogRef<DeletePatientComponent>,
    private patientService: PatientsService) { }

  ngOnInit(): void {
  }

  deletePatient(){
    this.dialogRef.close();
    this.patientService.deletePatient(this.username).subscribe();
  }

  closeDialog(){
    this.dialogRef.close();
  }
}
