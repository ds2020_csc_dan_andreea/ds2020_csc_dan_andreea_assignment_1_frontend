import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Medication } from 'src/app/models/Medication';
import { MedicationsService } from 'src/app/services/medications.service';
import { MedicationsViewComponent } from '../medications-view/medications-view.component';

@Component({
  selector: 'app-new-medication',
  templateUrl: './new-medication.component.html',
  styleUrls: ['./new-medication.component.css']
})
export class NewMedicationComponent implements OnInit {

  name: string="";
  dosage: string="";
  sideEffects: string="";

  constructor(public dialogRef: MatDialogRef<NewMedicationComponent>,
    private medicationService: MedicationsService) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dialogRef.close();
  }

  insertMedication(){
    let newMedication = new Medication();
    newMedication.name = this.name;
    newMedication.dosage = this.dosage;
    newMedication.sideEffects = this.sideEffects.split(',');
   
    this.closeDialog();
    return this.medicationService.insertMedication(newMedication).subscribe();
  }
}
