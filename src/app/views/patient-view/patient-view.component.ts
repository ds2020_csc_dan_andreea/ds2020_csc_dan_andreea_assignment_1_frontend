import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Patient } from 'src/app/models/Patient';
import { PatientsService } from 'src/app/services/patients.service';
import { DeletePatientComponent } from '../delete-patient/delete-patient.component';
import { EditPatientComponent } from '../edit-patient/edit-patient.component';
import { NewPacientComponent } from '../new-pacient/new-pacient.component';

@Component({
  selector: 'app-patient-view',
  templateUrl: './patient-view.component.html',
  styleUrls: ['./patient-view.component.css']
})
export class PatientViewComponent implements OnInit {

  role!: string;
  error!: string;
  patients: Patient[] = [];
  displayedColumns: string[] = ['id', 'address', 'birthdate','gender','medrecord','name'];
  dataSource = new MatTableDataSource<Patient>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private patientsService: PatientsService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.role=JSON.parse(localStorage.getItem('currentUser')!)['role'];

    if(this.role=='caregiver'){
      this.populateCaregiverPatients();
    }
    else if(this.role=='doctor'){
      this.displayedColumns = ['id', 'username', 'address', 'birthdate', 'cusername','gender','medrecord','name'];
      this.populatePatients();
    }

    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  populateCaregiverPatients(){
    this.patientsService.getPatientsForCaregiver().subscribe(
      (patients: Patient[]) => { this.setPatientsTable(patients); },
      (err: any) => console.log(err)
    );
  }

  populatePatients(){
    this.patientsService.getPatients().subscribe(
      (patients: Patient[]) => { this.setPatientsTable(patients); },
      (err: any) => console.log(err)
    ) 
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setPatientsTable(patients: Patient[]){
  this.dataSource = new MatTableDataSource<Patient>(patients);
  }

  insertPatient(){
    let dialogCreate = this.dialog.open(NewPacientComponent, {
      width: '400px',
      data: {}
    });
    dialogCreate.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  deletePatient(){
    let dialogDelete = this.dialog.open(DeletePatientComponent,{
      width: '400px',
      data: {}
    })
    dialogDelete.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  updatePatient(){
    let dialogUpdate = this.dialog.open(EditPatientComponent,{
      width: '400px',
      data: {}
    })
    dialogUpdate.afterClosed().subscribe(result => { this.ngOnInit() });
  }
}

