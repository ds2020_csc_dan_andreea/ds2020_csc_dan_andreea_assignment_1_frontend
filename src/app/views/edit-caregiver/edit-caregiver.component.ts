import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Caregiver } from 'src/app/models/Caregiver';
import { CaregiversService } from 'src/app/services/caregivers.service';
import { NewCaregiverComponent } from '../new-caregiver/new-caregiver.component';

@Component({
  selector: 'app-edit-caregiver',
  templateUrl: './edit-caregiver.component.html',
  styleUrls: ['./edit-caregiver.component.css']
})
export class EditCaregiverComponent implements OnInit {

  address:string="";
  birthdate:string="";
  cusername: string="";
  gender: string="";
  medrecord: string="";
  name: string="";
  role: string="";
  username: string="";

  constructor(public dialogRef: MatDialogRef<NewCaregiverComponent>,
    private caregiverService: CaregiversService) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dialogRef.close();
  }

  editCaregiver(){
    let newCaregiver = new Caregiver();
    newCaregiver.address= this.address;
    newCaregiver.birthdate = this.birthdate;
    newCaregiver.gender = this.gender;
    newCaregiver.name=this.name;
    newCaregiver.role="caregiver";
    newCaregiver.username=this.username;

    this.closeDialog();
    return this.caregiverService.editCaregiver(newCaregiver).subscribe();
  }

}
