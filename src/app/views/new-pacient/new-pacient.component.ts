import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Patient } from 'src/app/models/Patient';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-new-pacient',
  templateUrl: './new-pacient.component.html',
  styleUrls: ['./new-pacient.component.css']
})
export class NewPacientComponent implements OnInit {
  address:string="";
  birthdate:string="";
  cusername: string="";
  gender: string="";
  medrecord: string="";
  name: string="";
  role: string="";
  username: string="";

  constructor(public dialogRef: MatDialogRef<NewPacientComponent>,
    private patientService: PatientsService) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dialogRef.close();
  }

  insertPatient(){
    let newPatient = new Patient();
    newPatient.address= this.address;
    newPatient.birthdate = this.birthdate;
    newPatient.cusername = this.cusername;
    newPatient.gender = this.gender;
    newPatient.medrecord = this.medrecord;
    newPatient.name=this.name;
    newPatient.role="patient";
    newPatient.username=this.username;

    this.closeDialog();
    return this.patientService.insertPatient(newPatient).subscribe();
  }

}
