import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCaregiverComponent } from './new-caregiver.component';

describe('NewCaregiverComponent', () => {
  let component: NewCaregiverComponent;
  let fixture: ComponentFixture<NewCaregiverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCaregiverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCaregiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
