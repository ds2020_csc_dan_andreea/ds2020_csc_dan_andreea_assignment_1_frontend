import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Caregiver } from 'src/app/models/Caregiver';
import { CaregiversService } from 'src/app/services/caregivers.service';

@Component({
  selector: 'app-new-caregiver',
  templateUrl: './new-caregiver.component.html',
  styleUrls: ['./new-caregiver.component.css']
})
export class NewCaregiverComponent implements OnInit {

  address:string="";
  birthdate:string="";
  cusername: string="";
  gender: string="";
  medrecord: string="";
  name: string="";
  role: string="";
  username: string="";

  constructor(public dialogRef: MatDialogRef<NewCaregiverComponent>,
    private caregiverService: CaregiversService) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dialogRef.close();
  }

  insertCaregiver(){
    let newCaregiver = new Caregiver();
    newCaregiver.address= this.address;
    newCaregiver.birthdate = this.birthdate;
    newCaregiver.gender = this.gender;
    newCaregiver.name=this.name;
    newCaregiver.role="caregiver";
    newCaregiver.username=this.username;

    this.closeDialog();
    return this.caregiverService.insertCaregiver(newCaregiver).subscribe();
  }

}
