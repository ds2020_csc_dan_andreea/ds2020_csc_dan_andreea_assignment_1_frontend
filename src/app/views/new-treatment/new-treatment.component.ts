import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TreatmentPlan } from 'src/app/models/TreatmentPlan';
import { TreatmentsService } from 'src/app/services/treatments.service';

@Component({
  selector: 'app-new-treatment',
  templateUrl: './new-treatment.component.html',
  styleUrls: ['./new-treatment.component.css']
})
export class NewTreatmentComponent implements OnInit {

  firsthour: string="";
  secondhour: string="";
  period: string="";
  pacient_username: string="";
  medications: string="";

  constructor(public dialogRef: MatDialogRef<NewTreatmentComponent>,
    private treatmentService: TreatmentsService) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dialogRef.close();
  }

  insertTreatment(){
    let newTreatment = new TreatmentPlan();
    newTreatment.firsthour = this.firsthour;
    newTreatment.secondhour = this.secondhour;
    newTreatment.period = this.period;
    newTreatment.pacient_username = this.pacient_username;
    newTreatment.medications = this.medications.split(',');

    this.closeDialog();
    return this.treatmentService.insertTreatment(newTreatment).subscribe();
  }

}
