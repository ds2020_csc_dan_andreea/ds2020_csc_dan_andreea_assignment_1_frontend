import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  role!: string;

  constructor(private router: Router, private authentication: AuthenticationService) { }

  ngOnInit(): void {
    this.role=JSON.parse(localStorage.getItem("currentUser")!)['role'];
  }

  home(){
    switch (this.role) {
      case ("patient").valueOf():
        this.router.navigate(['/patient']); // route to patient view
        break;
      case ("caregiver").valueOf():
        this.router.navigate(['/caregiver']); // route to caregiver view
        break;
      case ("doctor").valueOf():
        this.router.navigate(['/doctor']); // route to doctor view
        break;
      default:
        this.router.navigate(['/login']);
    }
  }

  logout(){
    this.authentication.logout();
    this.router.navigate(['']);
  }

  getTreatmentPlans(){
    this.router.navigate(['/treatmentsView']);
  }

  getPatients(){
    this.router.navigate(['/patientsView']);
  }

  getCaregivers(){
    this.router.navigate(['/caregiversView']);
  }

  getMedications(){
    this.router.navigate(['/medicationsView']);
  }

}
