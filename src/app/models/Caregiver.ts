export class Caregiver{
    id!: string;
    address!: string;
    birthdate!: string;
    gender!: string;
    name!: string;
    role!: string;
    username!: string;
}