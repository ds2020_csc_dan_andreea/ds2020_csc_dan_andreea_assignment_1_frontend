export class Patient{
    id!: string;
    address!: string;
    birthdate!: string;
    cusername!: string;
    gender!: string;
    medrecord!: string;
    name!: string;
    role!: string;
    username!: string;
}