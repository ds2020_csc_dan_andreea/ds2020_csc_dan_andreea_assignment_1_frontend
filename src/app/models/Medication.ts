export class Medication{
    name!: string;
    dosage!: string;
    sideEffects!: string[];
}