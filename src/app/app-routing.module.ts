import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaregiverGuardGuard } from './guards/caregiver-guard.guard';
import { DoctorGuardGuard } from './guards/doctor-guard.guard';
import { PatientGuardGuard } from './guards/patient-guard.guard';
import { SyncGuardHelperGuard } from './guards/sync-guard-helper.guard';
import { CaregiverViewComponent } from './homeViews/caregiver-view/caregiver-view.component';
import { DoctorViewComponent } from './homeViews/doctor-view/doctor-view.component';
import { PacientViewComponent } from './homeViews/pacient-view/pacient-view.component';
import { LoginComponent } from './login/login.component';
import { Login } from './models/Login';
import { NavbarComponent } from './navbar/navbar.component';
import { CaregiversViewComponent } from './views/caregivers-view/caregivers-view.component';
import { MedicationsViewComponent } from './views/medications-view/medications-view.component';
import { PatientViewComponent } from './views/patient-view/patient-view.component';
import { TreatmentViewComponent } from './views/treatment-view/treatment-view.component';

const routes: Routes = [
  {
    path:"login",
    component: LoginComponent
  },
  {
    path:"navbar",
    component: NavbarComponent
  },
  {
    path:"patient",
    component: PacientViewComponent,
    canActivate: [PatientGuardGuard] 
  },
  {
    path:"doctor",
    component: DoctorViewComponent,
    canActivate: [DoctorGuardGuard]
  },
  {
    path:"caregiver",
    component: CaregiverViewComponent,
    canActivate: [CaregiverGuardGuard]
  },
  {
    path:"treatmentsView",
    component: TreatmentViewComponent
  },
  {
    path:"patientsView",
    component: PatientViewComponent
  },
  {
    path:"caregiversView",
    component: CaregiversViewComponent
  },
  {
    path:"medicationsView",
    component: MedicationsViewComponent,
    canActivate: [DoctorGuardGuard]
  },
  {
    path:"",
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
