import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-caregiver-view',
  templateUrl: './caregiver-view.component.html',
  styleUrls: ['./caregiver-view.component.css']
})
export class CaregiverViewComponent implements OnInit {
  name!: string;
  birthdate!: string;
  gender!: string;
  address!: string;
  username!: string;
  role!: string;

  constructor() { }

  ngOnInit(): void {
    this.name= JSON.parse(localStorage.getItem("currentUser")!)['name'];
    this.birthdate= JSON.parse(localStorage.getItem("currentUser")!)['birthdate'];
    this.gender= JSON.parse(localStorage.getItem("currentUser")!)['gender'];
    this.address= JSON.parse(localStorage.getItem("currentUser")!)['address'];
    this.username= JSON.parse(localStorage.getItem("currentUser")!)['username'];
    this.role= JSON.parse(localStorage.getItem("currentUser")!)['role'];
  }

}
