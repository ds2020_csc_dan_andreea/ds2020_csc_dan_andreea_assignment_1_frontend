import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverViewComponent } from './caregiver-view.component';

describe('CaregiverViewComponent', () => {
  let component: CaregiverViewComponent;
  let fixture: ComponentFixture<CaregiverViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaregiverViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
