import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pacient-view',
  templateUrl: './pacient-view.component.html',
  styleUrls: ['./pacient-view.component.css']
})
export class PacientViewComponent implements OnInit {
  name!: string;
  birthdate!: string;
  gender!: string;
  address!: string;
  medrecord!: string;
  caregiver_username!: string;
  username!: string;
  role!: string;

  constructor() { }

  ngOnInit(): void {
    this.getUserDetails();
  }

  getUserDetails(){
    this.name=JSON.parse(localStorage.getItem('currentUser')!)['name'];
    this.birthdate=JSON.parse(localStorage.getItem('currentUser')!)['birthdate'];
    this.gender=JSON.parse(localStorage.getItem('currentUser')!)['gender'];
    this.address=JSON.parse(localStorage.getItem('currentUser')!)['address'];
    this.medrecord=JSON.parse(localStorage.getItem('currentUser')!)['medrecord'];
    this.caregiver_username=JSON.parse(localStorage.getItem('currentUser')!)['cusername'];
    this.username=JSON.parse(localStorage.getItem('currentUser')!)['username'];
    this.role=JSON.parse(localStorage.getItem('currentUser')!)['role'];
  }

}
