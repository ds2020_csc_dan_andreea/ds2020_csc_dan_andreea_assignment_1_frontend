import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-view',
  templateUrl: './doctor-view.component.html',
  styleUrls: ['./doctor-view.component.css']
})
export class DoctorViewComponent implements OnInit {
  id!: string;
  username!: string;
  role!: string;

  constructor() { }

  ngOnInit(): void {
    this.getDoctorDetails();
  }

  getDoctorDetails(){
    this.id= JSON.parse(localStorage.getItem("currentUser")!)['id'];
    this.username = JSON.parse(localStorage.getItem("currentUser")!)['username'];
    this.role = JSON.parse(localStorage.getItem("currentUser")!)['role'];
  }
}
