import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {LoginComponent } from './login/login.component';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import {MatInputModule} from '@angular/material/input';
import { PacientViewComponent } from './homeViews/pacient-view/pacient-view.component';
import { TreatmentViewComponent } from './views/treatment-view/treatment-view.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DoctorViewComponent } from './homeViews/doctor-view/doctor-view.component';
import { CaregiverViewComponent } from './homeViews/caregiver-view/caregiver-view.component';
import { PatientViewComponent } from './views/patient-view/patient-view.component';
import { CaregiversViewComponent } from './views/caregivers-view/caregivers-view.component';
import { MedicationsViewComponent } from './views/medications-view/medications-view.component';
import { NewPacientComponent } from './views/new-pacient/new-pacient.component';
import { NewCaregiverComponent } from './views/new-caregiver/new-caregiver.component';
import { NewMedicationComponent } from './views/new-medication/new-medication.component';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NewTreatmentComponent } from './views/new-treatment/new-treatment.component';
import { DeletePatientComponent } from './views/delete-patient/delete-patient.component';
import { DeleteCaregiverComponent } from './views/delete-caregiver/delete-caregiver.component';
import { DeleteMedicationComponent } from './views/delete-medication/delete-medication.component';
import { EditPatientComponent } from './views/edit-patient/edit-patient.component';
import { EditCaregiverComponent } from './views/edit-caregiver/edit-caregiver.component';
import { EditMedicationComponent } from './views/edit-medication/edit-medication.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    PacientViewComponent,
    TreatmentViewComponent,
    DoctorViewComponent,
    CaregiverViewComponent,
    PatientViewComponent,
    CaregiversViewComponent,
    MedicationsViewComponent,
    NewPacientComponent,
    NewCaregiverComponent,
    NewMedicationComponent,
    NewTreatmentComponent,
    DeletePatientComponent,
    DeleteCaregiverComponent,
    DeleteMedicationComponent,
    EditPatientComponent,
    EditCaregiverComponent,
    EditMedicationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    HttpClientModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
